const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../collections/user');
const Request = require('../collections/requests');
const config = require('../config/database');


router.post('/addReq', (req, res, next) => {
    let newReq = new Request({
        postedBy: {
            id: req.body.postedBy
        },
        title: req.body.title,
        desc: req.body.desc,
        postedOn: new Date(),
        expectedOn: req.body.expectedOn,
        status: "live"

    });


    Request.addReq(newReq, (err, req) => {
        if (err) {
            res.json({
                success: false,
                msg: 'Failed to post req.',
                error: err
            })
        } else {
            res.json({
                success: true,
                msg: 'Posted Req.'
            });
        }
    });
});
router.get('/getReq', passport.authenticate('jwt', {
    session: false
}), (req, res, next) => {
    Request.find({}, function(err, result) {
        if (err)
            return res.send(err);
        res.send({
            result
        });
    });
});

router.post('/addComment',(req,res,next) => {
  const newComment = ({
    postedBy:{
      id:req.body.postedBy
    },
    reqId : req.body.reqId,
    comment: req.body.comment,
    when: new Date()
  });
  Request.addComment(newComment,(err,comment)=> {
    if (err){
      throw err;
      res.json({
        success:false,
        msg:"Falided updating Comment"
      });
      return false;
    }
  else{
      res.json({
        success:true,
        msg:"Comment added Successfully"
      });
      return true;
    }
  })
});

module.exports = router;
