const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../collections/user');
const config = require('../config/database');


// Register
router.post('/register', (req, res, next) => {
    let newUser = new User({
        name: {
            firstName: req.body.firstName,
            lastName: req.body.lastName
        },
        dateOfBirth: req.body.dateOfBirth,
        phoneNumber: req.body.phoneNumber,
        emailAddress: req.body.emailAddress,
        handle: req.body.handle,
        password: {
            currentPassword: {
                password: req.body.password,
                setOn: new Date()
            },
        },
        gender:req.body.gender,
        joinedOn: new Date(),
    });
    User.addUser(newUser, (err, user) => {
        if (err) {
            res.json({
                success: false,
                msg: 'not registered',
                error: err
            });
        } if (user) {
            res.json({
                success: true,
                msg: 'registered..'
            });
        }
    });
});


// Authenticate {login}
router.post('/login', (req, res, next) => {
    // getting email and password from user
    let uname = req.body.user;
    let password = req.body.password;
    // Function to process after getting user...
    gotUser = (err, user ) => {
        if (err) {
            throw err;
        }
        if (!user) {
            res.json({
                success: false,
                msg: 'User not found..'
            });
        }
        if(user){
        User.comparePassword(password, user.password.currentPassword.password, (err, isMatch) => {
            if (err) {
                throw err;
            }
            if (isMatch) {
                const token = jwt.sign(user, config.secret, {
                    expiresIn: 604800
                });
                User.recordLogin(user);
                res.json({
                    success: true,
                    token: 'JWT ' + token,
                    user: {
                        user : user
                    }
                });
                return true;
            }
            res.json({
                success: false,
                msg: 'Wrong Password..'
            });
        });
    }
}

    if (typeof(uname) === 'string') {
        if (/@/.test(uname)) {
            User.getUserByEmail(uname, gotUser);
        } else {
            User.getUserByHandle(uname, gotUser);
        }
    } else {
        User.getUserByPhone(uname,gotUser);
    }
});


router.get('/profile', passport.authenticate('jwt', {
    session: false
}), (req, res, next) => {
    res.json({
        user: req.user
    });
});

module.exports = router;
