const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');
const user = require('./routes/user');
const req = require('./routes/request');


const app = express();
const port = 3030;


// Connecting to DataBase...
mongoose.Promise = require('bluebird');
mongoose.connect(config.database);

// On connection...
mongoose.connection.on('connected', () => {
    console.log(`Connected to Database ${config.database}`);
});

//  On error...
mongoose.connection.on('error', (err) => {
    console.log(`DataBase Connection error ${err}`);
});


//use cors..
app.use(cors());

// Settin static folder...
app.use(express.static(path.join(__dirname, 'public')));

// Bodyparser Middleware...
app.use(bodyParser.json());


// Passport middleware...
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);

// Index Route...
app.get('/', (req, res) => {
    res.send('Invalid Endpoint...');
});

//Connecting the user router...
app.use('/user', user);

// connecting to request route
app.use('/req', req);

// Start server...
app.listen(port, () => {
    console.log(`Listening at port ${port}`);
});