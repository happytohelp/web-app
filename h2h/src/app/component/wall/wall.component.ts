import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { PipeTransform, Pipe } from '@angular/core';



@Component({
	selector: 'app-wall',
	templateUrl: './wall.component.html',
	styleUrls: ['./wall.component.css']
})
export class WallComponent implements OnInit {

	title: String;
	desc: String;
	date: Number;
	month: Number;
	year: Number;
	hour: Number;
	minute: Number;
	requests=[];
	name: {};
	constructor(private router: Router, private authService: AuthService) { }
	ngOnInit() {
		this.authService.getReq().subscribe(result => {
			this.requests=result.result;
		},
			err => {
				console.log(err);
				return false;
			});
	}

onAddReqClick(){
	$('#myModal').modal('toggle')
}
onAddReqSubmitClick(){
	const req = {
		postedBy: JSON.parse(localStorage.getItem('UID')),
		title: this.title,
		expectedOn: this.year + '/' + this.month + '/' + this.date + ' ' + this.hour + ':' + this.minute,
		desc: this.desc
	}
	// Post Request
	this.authService.addReq(req).subscribe(data => {
		if (data.success) {
			console.log('Updated..');
		} if (!data.success) {
			console.log('Not Updated ' + data.msg + data.err);
		}
	});


}
}
// @Pipe({name: 'keys'})
// export class KeysPipe implements PipeTransform {
//   transform(value, args:string[]) : any {
//     let keys = [];
//     for (let key in value) {
//       keys.push(key);
//     }
//     return keys;
//   }
// }