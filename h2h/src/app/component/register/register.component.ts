import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import {ValidateService} from '../../services/validate.service';
import {FlashMessagesService} from 'angular2-flash-messages';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	firstName:String;
	lastName:String;
	handle:String;
	emailAddress:String;
	password:String;
	phoneNumber:Number;
	gender:String;
	date:Number;
	month:Number;
	year:Number;

  constructor(private router: Router,private flashMessage : FlashMessagesService, private authService : AuthService, private  validateService: ValidateService) { }

  ngOnInit() {
  }
  onRegisterSubmit(){
  	const user = {
  			firstName : this.firstName,
  			lastName: this.lastName,
  		emailAddress : this.emailAddress,
  		phoneNumber : this.phoneNumber,
  		dateOfBirth : this.year+'/'+this.month+'/' +this.date,
  	  password:this.password,
  		gender:this.gender,
  		handle:this.handle
  	}
    // Validate Input
    if(!this.validateService.validateRegister(user)){
      this.flashMessage.show('Please Fill in all fields',{cssClass: 'alert-danger',timeout:3000});
      return false;
    }

    // Check Email Pattern
    if(!this.validateService.validateEmail(user.emailAddress)){
      this.flashMessage.show('Please enter a valid email', {cssClass: 'alert-danger',timeout:3000});
      return false;
    }
  	// Register User
  	this.authService.registerUser(user).subscribe(data => {
  		if(data.success){
        this.flashMessage.show('You are now registered... n can login now', {cssClass: 'alert-success',timeout:3000});
  			this.router.navigate(['/login']);
  		}else{
        this.flashMessage.show('Something went wrong', {cssClass: 'alert-danger',timeout:3000});
  			this.router.navigate(['/register']);
  		}
  	});
  }

}
