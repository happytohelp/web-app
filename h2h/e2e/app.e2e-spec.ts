import { H2hPage } from './app.po';

describe('h2h App', () => {
  let page: H2hPage;

  beforeEach(() => {
    page = new H2hPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
