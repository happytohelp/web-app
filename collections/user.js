const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User collection Schema...
const userSchema = mongoose.Schema({
    name: {
        firstName: {
            type: String,
            required: true
        },
        lastName: {
            type: String,
            required: true
        }
    },
    dateOfBirth: {
        type: Date,
        required: true
    },
    phoneNumber: {
        type: Number,
        required: true,
        unique: true
    },
    emailAddress: {
        type: String,
        required: true,
        unique: true
    },
    handle: {
        type: String,
        required: true,
        unique: true
    },
    gender: {
        type: String,
        required: true,
    },
    social: {
        fb: {
            type: String
        },
        twitter: {
            type: String
        },
        google: {
            type: String
        }
    },
    password: {
        currentPassword: {
            password: {
                type: String,
                required: true,
            },
            setOn: {
                type: Date
            }
        },
        oldPassword: {
            password: {
                type: String
            },
            changedOn: {
                type: Date
            }
        }

    },
    followers: [{
        type: String
    }],
    following: [{
        type: String
    }],
    joinedOn: {
        type: Date
    },
    lastLogin: [{
        type: Date
    }],
    ratings: [{
        requestID: {
            type: String
        },
        stars: {
            type: Number
        },
        review: {
            type: String
        }
    }],
    helps: [{
        type: String
    }],
    badges: [{
        type: String
    }]
});

const User = module.exports = mongoose.model('user', userSchema);

module.exports.addUser = (newUser, callback) => {
    let pass = newUser.password.currentPassword.password;
    bcrypt.genSalt(12, (err, salt) => {
        bcrypt.hash(pass, salt, (err, hash) => {
            if (err)
                throw err;
            else {
                newUser.password.currentPassword.password = hash;
                newUser.save(callback);
            }
        });
    });
}

// Get user by handle
module.exports.getUserByHandle = (handle, callback) => {
    const query = {
        handle: handle
    };
    User.findOne(query, callback);
}

// Get user by Phone
module.exports.getUserByPhone = (phone, callback) => {
    const query = {
        phoneNumber: phone
    };
    User.findOne(query, callback);
}

// Get user by Email
module.exports.getUserByEmail = (email, callback) => {
    const query = {
        emailAddress: email
    };
    User.findOne(query, callback);
}

// Get user by ID
module.exports.getUserById = (id,callback) =>{
    const query ={
        _id:id
    };
    User.findOne(query,callback);
}

module.exports.comparePassword = (password, hash, callback) => {
    bcrypt.compare(password, hash, (err, isMatch) => {
        if (err) {
            throw err;
        }
        callback(null, isMatch);
    });
}


// This function enables us to record the lastLogin limited to 10
module.exports.recordLogin = (user) => {
    let uid = user._id;
    User.findByIdAndUpdate(
        uid, {
            $push: {
                lastLogin: {
                    $each: [new Date],
                    $slice: -10
                }
            }
        }, {
            new: true
        },
        (err, modle) => {
            if (err) {
                console.log(err);
            }
        }
    );
  }
