const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');
const User = require('./user');

const reqSchema = mongoose.Schema({
    postedBy: {
        id: {
            type: String,
            required: true
        },
        name: {
            firstName: {
                type: String,
                required: true
            },
            lastName: {
                type: String,
                required: true
            }
        },
        handle: {
            type: String,
            required: true
        }
    },
    title: {
        type: String,
        required: true
    },
    desc: {
        type: String
    },
    expectedOn: {
        type: Date,
        required: true
    },
    accepted: {
        type: String,
    },
    following: [{
        uid: {
            type: String
        }
    }],
    postedOn: {
        type: Date,
        required: true
    },
    comments: [{
        postedBy: {
            id: {
                type: String,
                required: true
            },
            name: {
                firstName: {
                    type: String,
                    required: true
                },
                lastName: {
                    type: String,
                    required: true
                }
            },
            handle: {
                type: String,
                required: true
            }
        },
        comment: {
            type: String,
            required: true
        },
        when: {
            type: Date,
            required: true
        }
    }],
    cancled: [{
        uid: {
            type: String
        }
    }],
    status: {
        type: String,
        required: true
    }

});
// Initializing mongoose model
const req = module.exports = mongoose.model('req', reqSchema);


module.exports.addReq = (newReq, callback) => {
        console.log(newReq);
        User.getUserById(newReq.postedBy.id, (err, user) => {
            if (err) {
                throw err;
            }
            if (user) {
                newReq.postedBy.name = user.name;
                newReq.postedBy.handle = user.handle;
                newReq.save(callback);
            }
        });
    }
    // Function to add a comment 
module.exports.addComment = (newComment, callback) => {
    User.getUserById(newComment.postedBy.id, (err, user) => {
        if (err) {
            throw err;
        }
        if (user) {
            newComment.postedBy.name = user.name;
            newComment.postedBy.handle = user.handle;
            let rid = newComment.reqId;
            req.findByIdAndUpdate(rid, {
                    $push: {
                        comments: newComment
                    }
                }, {
                    new: true
                },
                (err, modle) => {
                    if (err) {
                        callback(err,false);
                    }
                    callback(null,true);
                }
            );
        }
    });
}