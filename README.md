# Happy To Help
Happy to help is a small initivate by people looking forward to contribute to the society; therefore -**_ Enhancing Humanity _** . This is an open source project looking forward for contributors and the code inscpectors :)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
1. [NODEjs](https://nodejs.org)
2. [MongoDB](https://www.mongodb.com)


### Installing

A step by step series of examples that tell you have to get a development env running

1. Clone this repository
```
git clone https://happytohelp@gitlab.com/happytohelp/web-app.git
```

2. Install the node packages
```
cd web-app
npm install
```

3. Start the Backend
```
nodemon
```

4. Start the FrontEnd
```
cd h2h
ng serve
```


## Built With

- [Angular 2+](http://www.angular.io) - The web framework used
- [NodeJs](https://www.nodejs.orh/) - Dependency Management
- [MongoDB](https://www.mongodb.com) - The Backend
